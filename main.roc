app "calculator"
    packages { pf: "https://github.com/roc-lang/basic-cli/releases/download/0.3.2/tE4xS_zLdmmxmHwHih9kHWQ7fsXtJr7W7h3425-eZFk.tar.br" }
    imports [pf.Stdout, pf.Stdin, pf.Task.{ Task, await }]
    provides [main] to pf

main =
    _ <- await (Stdout.line "Hello")
    Task.loop initialState prompt

State : List I64
initialState : State
initialState = []

Op : [Plus, Times]

Input : [Number I64, Op Op, Done]
parseInput : Str -> Result Input [ParseError]
parseInput = \s ->
    when s is
        "quit" -> Ok Done
        "+" -> Ok (Op Plus)
        "*" -> Ok (Op Times)
        _ ->
            when Str.toI64 s is
                Ok number -> Ok (Number number)
                Err _ -> Err ParseError

prompt : State -> Task [Step State, Done {}] *
prompt = \state ->
    _ <- await (Stdout.line (stateToString state))
    _ <- await (Stdout.write "> ")
    rawInput <- await Stdin.line
    when parseInput rawInput is
        Ok Done ->
            _ <- await (Stdout.line "Bye!")
            Task.succeed (Done {})

        Ok (Op op) ->
            when applyOp state op is
                Ok newState ->
                    Task.succeed (Step newState)

                Err StackEmpty ->
                    _ <- await (Stdout.line "Stack too small")
                    Task.succeed (Step state)

        Ok (Number number) ->
            newState = List.append state number
            Task.succeed (Step newState)

        Err ParseError ->
            _ <- await (Stdout.line "ParseError")
            Task.succeed (Step state)

stateToString : State -> Str
stateToString = \state ->
    Str.joinWith
        [
            "[",
            state
            |> List.map Num.toStr
            |> Str.joinWith ", ",
            "]",
        ]
        ""

applyOp : State, Op -> Result State [StackEmpty]
applyOp = \state, op ->
    when splitLastTwo state is
        Ok (rest, x, y) ->
            result =
                when op is
                    Plus ->
                        x + y

                    Times ->
                        x * y
            Ok (List.append rest result)

        Err TooSmall ->
            Err StackEmpty

splitLastTwo : List a -> Result (List a, a, a) [TooSmall]
splitLastTwo = \a ->
    when List.takeLast a 2 is
        [x, y] ->
            Ok (a |> List.dropLast |> List.dropLast, x, y)

        _ ->
            Err TooSmall
